# Use Nginx to directly serve ACME challenge responses using the
# provided snippet

# bind the webserver to the default listening address
sed -i 's|^listen\s*=|#&|' /etc/lacme/lacme.conf

DEBIAN_FRONTEND="noninteractive" apt install -y --no-install-recommends nginx-light curl
cat >/etc/nginx/sites-enabled/default <<-EOF
	server {
	    listen 80 default_server;
	    server_name _;
	    include /etc/lacme/nginx-static.conf;
	}
EOF
nginx

# 'challenge-directory' set to a non-existent directory
sed -ri 's|^#?challenge-directory\s*=.*|challenge-directory = /var/www/acme-challenge|' /etc/lacme/lacme.conf
! lacme newOrder 2>"$STDERR" || fail
grepstderr -Fqx "opendir(/var/www/acme-challenge): No such file or directory"

# ensure that requests to the root URI and challenge URIs respectively yield 403 Forbidden (no index) and 404 Not Found
install -o_lacme-client -gwww-data -m0750 -d /var/www/acme-challenge
rv="$(curl -w"%{http_code}" -so/dev/null http://127.0.0.1/.well-known/acme-challenge/)";    [ $rv -eq 403 ]
rv="$(curl -w"%{http_code}" -so/dev/null http://127.0.0.1/.well-known/acme-challenge/foo)"; [ $rv -eq 404 ]

# 'challenge-directory' set to a non-empty directory
touch /var/www/acme-challenge/.stamp
! lacme newOrder 2>"$STDERR" || fail
grepstderr -Fqx "Error: Refusing to use non-empty challenge directory /var/www/acme-challenge"

rm -f /var/www/acme-challenge/.stamp
lacme --debug newOrder 2>"$STDERR" || fail
test /etc/lacme/simpletest.rsa.crt -nt /etc/lacme/simpletest.rsa.key

ngrepstderr -Fq "Forking ACME webserver"
grepstderr  -Fq "Using existing webserver on /var/www/acme-challenge"
grepstderr  -Fq "Forking lacme-accountd, child PID "
grepstderr  -Fq "Forking /usr/libexec/lacme/client, child PID "
grepstderr  -Fq "Shutting down lacme-accountd"
ngrepstderr -Fq "Shutting down ACME webserver"
ngrepstderr -Eq "Incoming connection( from \S+)?: GET /\.well-known/acme-challenge/\S+ HTTP/[0-9.]+$"

# ensure nginx was indeed used to serve challenge responses (Let's Encrypt caches validation results)
grep -E "\"GET /\.well-known/acme-challenge/\S+ HTTP/[0-9.]+\" 200 .* \(([^)]+; )*Let's Encrypt validation server(; [^)]+)*\)\"$" \
    /var/log/nginx/access.log

# vim: set filetype=sh :
