# %-specifiers expansion

# lacme --config=, all specifiers, root privileges
! lacme --config="%C %E %t %h %T %g %G %u %U %%.conf" account 2>"$STDERR" || fail
grepstderr -Fxq "Can't open /var/cache /etc /run /root /tmp root 0 root 0 %.conf: No such file or directory"

# lacme --config=, all specifiers, root privileges, defined XDG_*
! env XDG_CACHE_HOME=/foo/cache XDG_CONFIG_HOME=/foo/config XDG_RUNTIME_DIR=/foo/run HOME=/foo/home USER=myuser TMPDIR=/foo/tmp \
    lacme --config="%C %E %t %h %T %g %G %u %U %%.conf" account 2>"$STDERR" || fail
grepstderr -Fxq "Can't open /var/cache /etc /run /root /foo/tmp root 0 root 0 %.conf: No such file or directory"

# lacme --config=, all specifiers, non-root, unset XDG_RUNTIME_DIR
! runuser -u nobody -- lacme --config="%C %E %t %h %T %g %G %u %U %%.conf" account 2>"$STDERR" || fail
grepstderr -Fxq "Error: Undefined expansion %t in \"%C %E %t %h %T %g %G %u %U %%.conf\""

# lacme --config=, all specifiers, non-root, defined XDG_RUNTIME_DIR, no other XDG_*
! runuser -u nobody -g www-data -- env XDG_RUNTIME_DIR=/foo/run \
    lacme --config="%C %E %t %h %T %g %G %u %U %%.conf" account 2>"$STDERR" || fail
grepstderr -Fxq "Can't open /nonexistent/.cache /nonexistent/.config /foo/run /nonexistent /tmp www-data 33 nobody 65534 %.conf: No such file or directory"

# lacme --config=, all specifiers, non-root, defined XDG_*
! runuser -u nobody -- env XDG_CACHE_HOME=/foo/cache XDG_CONFIG_HOME=/foo/config XDG_RUNTIME_DIR=/foo/run HOME=/foo/home USER=myuser TMPDIR=/foo/tmp \
    lacme --config="%C %E %t %h %T %g %G %u %U %%.conf" account 2>"$STDERR" || fail
grepstderr -Fxq "Can't open /foo/cache /foo/config /foo/run /nonexistent /foo/tmp nogroup 65534 nobody 65534 %.conf: No such file or directory"

# lacme --socket=
! lacme --config="%E/lacme/lacme.conf" --socket="%t/S.lacme2" account --debug 2>"$STDERR" || fail
grepstderr -Fxq "Using configuration file: /etc/lacme/lacme.conf"
grepstderr -Fxq "Can't stat /run/S.lacme2: No such file or directory (Is lacme-accountd running?)"

# 'challenge-directory' setting (expands before privilege drop)
sed -ri 's|^#?challenge-directory\s*=.*|challenge-directory = /nonexistent/%u:%g|' /etc/lacme/lacme.conf
! lacme newOrder --debug 2>"$STDERR" || fail
grepstderr -Fq "Using existing webserver on /nonexistent/root:root"

# lacme --config-certs= and 'config-certs' settings (expands before privilege drop)
! lacme newOrder --debug nonexistent 2>"$STDERR" || fail
grepstderr -Fxq "Reading /etc/lacme/lacme-certs.conf"

sed -ri 's|^#?config-certs\s*=.*|config-certs = /nonexistent/%u:%g.conf|' /etc/lacme/lacme.conf
! lacme newOrder --debug nonexistent 2>"$STDERR" || fail
grepstderr -Fxq "Reading /nonexistent/root:root.conf"

! lacme newOrder --config-certs="%E/lacme/certs.conf.d" --debug nonexistent 2>"$STDERR" || fail
grepstderr -vFxq "Reading /etc/lacme/lacme-certs.conf"
grepstderr -Fxq "Reading /etc/lacme/certs.conf.d"

# 'config' setting in [accountd] section (expands after privilege drop)
sed -ri 's|^#?config\s*=\s*$|config = /nonexistent/%u:%g.conf|' /etc/lacme/lacme.conf
! lacme account 2>"$STDERR" || fail
grepstderr -Fxq "Error: Failed to open file '/nonexistent/root:root.conf' for reading: No such file or directory"

sed -ri 's|^#?user\s*=\s*$|user = nobody|' /etc/lacme/lacme.conf
! lacme account 2>"$STDERR" || fail
grepstderr -Fxq "Error: Failed to open file '/nonexistent/nobody:root.conf' for reading: No such file or directory"

# 'command' setting in [accountd] section (expands after privilege drop)
sed -ri 's|^#?command\s*=.*/lacme-accountd$|command = /usr/bin/lacme-accountd --%u|' /etc/lacme/lacme.conf
! lacme account 2>"$STDERR" || fail
grepstderr -Fxq "Unknown option: nobody"

sed -ri 's|^#?command\s*=.*/lacme-accountd .*|command = /nonexistent/%u/%g %u %g|' /etc/lacme/lacme.conf
! lacme account 2>"$STDERR" || fail
grepstderr -Eq "^Can't exec \"/nonexistent/nobody/root\": No such file or directory"


#######################################################################

# lacme-accountd --config=, all specifiers, root privileges
! lacme-accountd --config="%C %E %t %h %T %g %G %u %U %%.conf" 2>"$STDERR" || fail
grepstderr -Fxq "Error: Failed to open file '/var/cache /etc /run /root /tmp root 0 root 0 %.conf' for reading: No such file or directory"

# lacme-accountd --config=, all specifiers, root privileges, defined XDG_*
! env XDG_CACHE_HOME=/foo/cache XDG_CONFIG_HOME=/foo/config XDG_RUNTIME_DIR=/foo/run HOME=/foo/home USER=myuser TMPDIR=/foo/tmp \
    lacme-accountd --config="%C %E %t %h %T %g %G %u %U %%.conf" 2>"$STDERR" || fail
grepstderr -Fxq "Error: Failed to open file '/var/cache /etc /run /root /foo/tmp root 0 root 0 %.conf' for reading: No such file or directory"

# lacme-accountd --config=, all specifiers, non-root, unset XDG_RUNTIME_DIR
! runuser -u nobody -- lacme-accountd --config="%C %E %t %h %T %g %G %u %U %%.conf" account 2>"$STDERR" || fail
grepstderr -Fxq "Error: Undefined expansion %t in \"%C %E %t %h %T %g %G %u %U %%.conf\""

# lacme-accountd --config=, all specifiers, non-root, defined XDG_RUNTIME_DIR, no other XDG_*
! runuser -u nobody -g www-data -- env XDG_RUNTIME_DIR=/foo/run \
    lacme-accountd --config="%C %E %t %h %T %g %G %u %U %%.conf" 2>"$STDERR" || fail
grepstderr -Fxq "Error: Failed to open file '/nonexistent/.cache /nonexistent/.config /foo/run /nonexistent /tmp www-data 33 nobody 65534 %.conf' for reading: No such file or directory"

# lacme-accountd --config=, all specifiers, non-root, defined XDG_*
! runuser -u nobody -- env XDG_CACHE_HOME=/foo/cache XDG_CONFIG_HOME=/foo/config XDG_RUNTIME_DIR=/foo/run HOME=/foo/home USER=myuser TMPDIR=/foo/tmp \
    lacme-accountd --config="%C %E %t %h %T %g %G %u %U %%.conf" 2>"$STDERR" || fail
grepstderr -Fxq "Error: Failed to open file '/foo/cache /foo/config /foo/run /nonexistent /foo/tmp nogroup 65534 nobody 65534 %.conf' for reading: No such file or directory"

# lacme-accountd --privkey=
! lacme-accountd --privkey="file:%h/lacme-accountd.key" --debug 2>"$STDERR" || fail
grepstderr -Fxq "Error: Can't open /root/lacme-accountd.key: No such file or directory"

# lacme-accountd, default socket location
lacme-accountd --debug 2>"$STDERR" & PID=$!
sleep 1
kill $PID || fail
wait || fail
grepstderr -Fxq "Using configuration file: /etc/lacme/lacme-accountd.conf"
grepstderr -Fxq "Starting lacme Account Key Manager at /run/S.lacme"
grepstderr -Fxq "Unlinking /run/S.lacme"

# lacme-accountd --config= --socket= --privkey=
ln -s lacme-accountd.conf /etc/lacme/accountd.conf
lacme-accountd --config="%E/lacme/accountd.conf" --socket="%t/S.lacme2" --privkey="file:%E/lacme/account.key" --debug 2>"$STDERR" & PID=$!
sleep 1
kill $PID || fail
wait || fail
grepstderr -Fxq "Using configuration file: /etc/lacme/accountd.conf"
grepstderr -Fxq "Starting lacme Account Key Manager at /run/S.lacme2"
grepstderr -Fxq "Unlinking /run/S.lacme2"

# lacme-accountd, custom 'socket' setting
sed -ri 's|^#?socket\s*=.*|socket = %t/S.lacme3|' /etc/lacme/lacme-accountd.conf
lacme-accountd --debug 2>"$STDERR" & PID=$!
sleep 1
kill $PID || fail
wait || fail
grepstderr -Fxq "Using configuration file: /etc/lacme/lacme-accountd.conf"
grepstderr -Fxq "Starting lacme Account Key Manager at /run/S.lacme3"
grepstderr -Fxq "Unlinking /run/S.lacme3"

# lacme-accountd, custom 'privkey' setting
sed -ri 's|^privkey\s*=.*|privkey = file:%h/lacme-accountd.key|' /etc/lacme/lacme-accountd.conf
! lacme-accountd --debug 2>"$STDERR" || fail
grepstderr -Fxq "Error: Can't open /root/lacme-accountd.key: No such file or directory"

# vim: set filetype=sh :
